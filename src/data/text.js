export default {
  title: {
    it: 'Esperimento sociale internazionale sulla vita in quarantena.',
    en: 'International social experiment on life during quarantine.',
    de: 'Internationales soziales Experiment zum Leben während der Quarantäne.'
  },
  introductionTitle: {
    it: 'Introduzione',
    en: 'Introduction',
    de: 'Einleitung'
  },
  answersTitle: {
    it: 'Risposte',
    en: 'Answers',
    de: 'Antworten'
  },
  keywords: {
    it: 'Parole chiave',
    en: 'Keywords',
    de: 'Schlüsselbegriffe'
  },
  song: {
    it: 'Brano',
    en: 'Song',
    de: 'Song'
  },
  age: {
    it: 'Età',
    en: 'Age',
    de: 'Alter'
  },
  job: {
    it: 'Lavoro',
    en: 'Job',
    de: 'Job'
  },
  location: {
    it: 'Residenza',
    en: 'Location',
    de: 'Standort'
  }
}