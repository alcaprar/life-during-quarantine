const csv = require("csvtojson")
const fs = require('fs')

const csvToJson = async (csvFilePath) => {
  const jsonArrayObject = await csv({
    delimiter: '\t'
  }).fromFile(csvFilePath)
  return jsonArrayObject
}


(async () => {
  const simpleFilePath = './it.csv'

  const answers = await csvToJson(simpleFilePath)

  const it = answers.map((item, index) => {
    const row = index + 2
    return {
      id: 'IT' + row,
      gender: item['Sesso'] === 'Maschio' ? 'M' : 'F',
      job: item['Professione'],
      age: item['Età'],
      location: 'Italia',
      answer: [{
        language: 'it',
        text: item["Quali aspetti sperimentati nella mia vita odierna in quarantena vorrei mantenere anche dopo il 31-12-2020, quando, probabilmente, l'attuale reclusione per COVID 19 sarà finita?"],
        original: true
      }, {
        language: 'en',
        text: item["Se possibile traduci la risposta sopra in inglese."] ? item["Se possibile traduci la risposta sopra in inglese."] : ''
      }, {
        language: 'de',
        text: ''
      }]
    }
  })

  console.log(JSON.stringify(it))

  fs.writeFileSync('prova.json', JSON.stringify(it))
})()