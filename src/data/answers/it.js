export default [{
  id: 'IT2',
  age: 59,
  gender: 'M',
  job: "Dirigente azienda",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "Più tempo con la mia famiglia. Più tempo per i miei hobbies. Mangiare meglio. Fare regolarmente attività fisica. ",
    original: true
  },{
    language: 'en',
    text: "More time with my family. More time for my hobbies. Eat better. Exercise regularly."
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT3',
  age: 63,
  gender: 'M',
  job: "Medico",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "Silenzio.  Riposo. Meditazione. ",
    original: true
  },{
    language: 'en',
    text: "Silence , resting, meditation"
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT4',
  age: 30,
  gender: 'F',
  job: "Ricercatrice",
  location: 'Spagna',
  answer: [{
    language: 'it',
    text: "- Leggere - Mangiare sano - Passare del tempo con il mio partner e il mio bambino - Comunicare con amici e parenti con più frequenza grazie ai sociale",
    original: true
  },{
    language: 'en',
    text: "- Reading - Healthy diet - Spending more quality time with my partner and my baby - Keeping in touch with family and friends more often thanks to social media"
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT6',
  age: 62,
  gender: 'F',
  job: "Insegnante",
  location: '',
  answer: [{
    language: 'it',
    text: "Recupero del tempo come spazio da vivere e non da attraversare. \"protinus vive\". Silenzio come forza della ripartenza e della riflessione. ",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT7',
  age: 63,
  gender: 'F',
  job: "Insegnante",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "C  cantare dai balconi O  odorare la natura  R  rimanere in silenzio O  orgogliosa di essere italiana N  non dimenticare di dire \"Ti voglio bene\" A  abbracciare un albero V  vivere semplicemente I   illuminarsi di gioia per le piccole cose R  respirare l'aria frizzante del mattino U  urlare di felicità quando tutto questo sarà finito S  salutare sempre con un sorriso",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT8',
  age: 66,
  gender: 'M',
  job: "Giornalista pensionato",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "A livello pratico la lettura e la mancanza di stress. A livello psicologico , la verifica di quanto ti possono mancare le persone più care e la voglia di tornare ad apprezzare gli incontri con gli amici, le camminate e le pedalate, i viaggi , spero si traducano in una capacità di apprezzare di più tutto questo.",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT9',
  age: 52,
  gender: 'M',
  job: "Journalist",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "Anythng",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT10',
  age: 59,
  gender: 'F',
  job: "Insegnante",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "Nothing",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT11',
  age: 63,
  gender: 'F',
  job: "Pensionata",
  location: '',
  answer: [{
    language: 'it',
    text: "Lettura e approfondire la spiritualità. Poesia, anima",
    original: true
  },{
    language: 'en',
    text: "Reading and studying in deep the spirituality. Poetry, soul."
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT12',
  age: 0,
  gender: 'F',
  job: "Neonata",
  location: 'Italia',
  answer: [{
    language: 'it',
    text: "La lettura dei libri, tanta attenzione solamente su di me.",
    original: true
  },{
    language: 'en',
    text: "The attention on me, reading books"
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'IT',
  age: 0,
  gender: '',
  job: "",
  location: '',
  answer: [{
    language: 'it',
    text: "",
    original: true
  },{
    language: 'en',
    text: ""
  },{
    language: 'de',
    text: ''
  },]
}]