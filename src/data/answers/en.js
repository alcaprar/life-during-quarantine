export default [{
  id: 'EN2',
  name: 'Marie Kristine',
  surname: 'Mikkelsen',
  age: 24,
  job: 'Student',
  gender: 'F',
  country: 'DENMARK',
  location: 'Denmark',
  motherTongue: 'GERMAN',
  answer: [{
    language: 'en',
    text: 'To be in the moment, to take my time to enjoy the little things, and to cook proper food.',
    original: true
  },{
    language: 'it',
    text: 'Essere nel momento, prendermi il mio tempo per godermi le piccole cose e cucinare cibo adeguato.'
  },{
    language: 'de',
    text: 'Im Moment zu sein, mir Zeit zu nehmen, die kleinen Dinge zu genießen und richtiges Essen zu kochen.'
  },]
}, {
  id: 'EN3',
  age: 25,
  job: 'Technician of service stations',
  gender: 'M',
  country: 'SPAIN',
  location: 'Spain',
  motherTongue: 'Catalan',
  answer: [{
    language: 'en',
    text: 'Continue saving money.',
    original: true
  },{
    language: 'it',
    text: 'Continuare a risparmiare.'
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'EN4',
  age: 24,
  job: 'EMPLOYEE HUMAN RESOURCES',
  gender: 'F',
  country: 'SPAIN',
  location: 'Spain',
  motherTongue: 'Catalan',
  answer: [{
    language: 'en',
    text: "honestly almost all, i do the same rutine as before, ( working, eating the same food, practing sport diferrently not at gym, but i'm constant at home). Maybe try not to be so consumerist, just buy what is necessary, not by sight",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'EN5',
  age: 20,
  job: "student",
  gender: 'M',
  country: 'Hong Kong',
  location: 'Hong Kong',
  motherTongue: 'English',
  answer: [{
    language: 'en',
    text: "wash hands, wear mask for preventive measure",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
}, {
  id: 'EN6',
  name: 'Ullah',
  surname: 'Alim',
  age: 50,
  job:'Pizza chef',
  gender: 'M',
  country: 'Bangladesh',
  location: 'Bangladesh',
  motherTongue: 'Bengali',
  answer: [{
    language: 'en',
    text: 'Washing hands, stay clean at all times.',
    original: true
  }, {
    language: 'it',
    text: 'Lavarsi le mani, rimanere sempre pulite.'
  }, {
    language: 'de',
    text: 'Hände waschen, immer sauber bleiben.'
  }]
}, {
  id: 'EN7',
  age: 26,
  job: "Consultant",
  gender: 'M',
  country: 'Denmark',
  location: 'Denmark',
  motherTongue: 'Italian',
  answer: [{
    language: 'en',
    text: "My girlfriend and I have started a number of fun and healthy habits during the lockdown which we would like to mantain in the long run: \
    1. We do a daily workout with our flatmates in the living room. We have purchased some yoga mats and follow some trainers on Youtube. Pretty simple. \
    2. I practice 10 minutes of meditation everyday. I use the app headspace and it is a game changer. \
    3. We cook nice, healthy vegetarian meals most times. We are allowed 1 meat serving per week. Lesson learned from a meat lover like me: you will not miss meat, it's only a matter of habits. I have always purchased meat so I kept doing it. But my body does not need meat everyday, hence I feel totally fine and my energy level is on par as before. \
    4. We have breakfast together everyday. We spend time making bowls with fresh fruit, nuts, oats. We brew coffee and toast bread. Best way to start the day. \
    5. We work on a very complex puzzle, maybe 15 to 30 minutes everyday \
    6. We are both learning Danish using the app Duolingo, and we have been doing 20 minutes practice everyday since the quarantine started All in all it seems to me that dropping a lot of \"task\" you normally have to do before/after work has allowed us to have a lot of time for \"us\". and our well-being So plenty of opportunities despite the tough times. ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN8',
  age: 55,
  job: "Artist",
  gender: 'F',
  country: '',
  location: 'Australia',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Exercise ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN9',
  age: 28,
  job: "Product Developer",
  gender: 'F',
  country: '',
  location: 'Switzerland',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Reaching out to friends in distance, Investing in self care (yoga, meditation, preparing nice meals), having some alone time, having gratitude for things we take for granted",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN10',
  age: 30,
  job: "Food science",
  gender: 'M',
  country: '',
  location: 'Greece',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Healthy lifestyle",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN11',
  age: 27,
  job: "Jobless",
  gender: 'M',
  country: '',
  location: 'Taiwan',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Playing video games",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN12',
  age: 45,
  job: "Employee",
  gender: 'F',
  country: '',
  location: 'Austria',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Keeping close contact with family and friends regulary, playing a lot with my child, reading good books, time for myself, take time for good coffee, daily sport, doing things immediatly not postponing",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN13',
  age: 25,
  job: "Decorator",
  gender: 'F',
  country: '',
  location: 'Italia',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Spending some time in the sun everyday; reading a lot; playing board games with my family; taking time to look up things which i am curious about",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN14',
  age: 42,
  job: "Admin",
  gender: 'F',
  country: '',
  location: 'Scotland',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Reading books",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN15',
  age: 24,
  job: "Student",
  gender: '',
  country: '',
  location: 'Italia',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Reading before bed, yoga and 30 min workout a day ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN16',
  age: 60,
  job: "Interpreter, translator, teacher",
  gender: 'F',
  country: '',
  location: 'USA',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Regularly reach out to people in person or via internet. ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN17',
  age: 66,
  job: "Photographer",
  gender: 'F',
  country: '',
  location: 'USA',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Phoning friends ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN18',
  age: 72,
  job: "Retired teacher",
  gender: 'F',
  country: '',
  location: 'USA',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Fewer outside activities/obligations",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
},{
  id: 'EN19',
  age: 71,
  job: "Attorney",
  gender: 'F',
  country: '',
  location: 'USA',
  motherTongue: '',
  answer: [{
    language: 'en',
    text: "Taking time for relaxation, reflection and meditation.  Communicating by Zoom, cards and letters with friends and family who live far away.  Feeling a connectivity with the rest of the world.   Spending less money.  Being more cognizant of the difference between wants and needs.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'de',
    text: ''
  },]
}]