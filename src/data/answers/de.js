export default [{
  id: 'DE2',
  age: 63,
  job: "Pensionist",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Das Märchen von Greta und Corona \
    Es war im Frühling 2020. Ein zwei Monate altes Mädchen Namens Greta und sein Opa durften sich nicht sehen und schon gar nicht umarmen. In ganz Italien und auf der halben Welt gab es die Verordnung “Bleibt zu Hause” und “Meidet den Kontakt mit anderen Menschen”. Alle Menschen mussten für Wochen und Wochen daheim bleiben. Dem Opa gefiel diese Distanz zwischen ihm und seinem Enkelkind überhaupt nicht. Und so entdeckte der Opa, der auf den mittelitalienischen Hügeln lebte ,  einige Dinge, die er seit Jahrzehnten nicht mehr machte: \
    Er begann sich über die Stille zu freuen. Der Lärm der Motorräder und Autos und viele weitere störende Geräusche waren plötzlich verschwunden. Das Zwitschern der Vögel und das Quaken der Frösche war deutlicher als eh und je. Der Opa lauschte oft stundenlang diesen Tönen nach, als sei es ein Frühlingskonzert von Giochino Rossini,  und träumte endlos von der Schönheit der Natur.  \
    Er staunte über die saubere Luft, die die Stadt eroberte und die enorme Fernsicht hinaus aufs Meer und hinein in die Hügellandschaft. Der Opa verfügte über viel Zeit und werkelte gerne im Garten herum.  Er  setzte eine Riesensonnenblumen, die bis zu 200 m hoch wurde. Und siehe da: der Opa, der mutig war wie ein Löwe,  kletterte eines Tages die Sonnenblume hoch und mit freiem Auge konnte er die Küste Kroatiens erkunden und - höre, höre - sogar das Haus und die Wiege von Greta sehen. Und nachts sichteten Oma und Opa viele neue Sterne, die sie ihr ganzes Leben lang noch nie gesehen hatten. Oft schliefen die beiden – umarmt - beim Sternezählen auf freier Wiese ein und deshalb wissen sie bis heute noch nicht wieviele Sterne am Firmament strahlen. \
    Die Oma überraschte den Opa  täglich mit hausgemachter Pizza, Vollkornbrot und Kartoffelnocken. Seit mehr als 20 Jahren gab es diese Speisen  nur mehr aus dem Supermarket “Coop” oder im  Restaurant “La Vecchia Cantina”, rund um die Ecke. Nun duftete die Küche der Großeltern wieder wunderbar nach diesen alten Köstlichkeiten. \
    Opa und Oma führten jeden Tag ihren  Hund “Ugo” aus. Falls  dieses Trio  einem Nachbarn über den Weg lief, so wichen sich alle in einem grossen Bogen aus. Nicht etwa weil “Ugo” bellte oder gar die Zähne fletschte,  nein,  sie hatten alle Angst vom  “gekrönten Virus”  angesteckt zu werden.  Und gleichzeitig grüssten sich dennoch alle freundlichst aus der Ferne. Und Opa quatschte oft stundenlang mit Alberto und Mariella ,versteckt und über den Gartenzaun hinweg, über Gott und die Welt. \
    Und sogar das Joggen war verboten: höchstens  200 Meter rund um das Haus waren erlaubt. Und so entdeckte der Opa erneut die Intervallläufe, wie in seiner Jugendzeit: 200 Meter schnell und 200 Meter langsam laufen und viele Wiederholungen,  und so wurde der Opa von Tag zu Tag schneller, elastischer und -vielleicht auch- jünger. Weiters hatte er seit Jahrzehnten keinen Bock mehr auf Turnübungen für seinen Bauch und seine Rücken: und nun, wo er unter Hausarrest stand, machten diese auf einmal Spass. \
    Die  Moral dieses Märchens ist ? \
    “Liebe Greta, falls der Opa wegen des Corona Virus nicht zum Zuhausebleiben verpflichtet worden wäre, falls er das Enkelkind  jeden Tag umarmt hätte, wäre er  vermutlich nie auf die Schnapsidee gekommen, für DICH GRETA dieses Märchen zu schreiben. Und sollte dieses Besuchsverbot noch länger  aufrecht erhalten bleiben, so erwarten dich noch weitere Märchen.”",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
},{
  id: 'DE3',
  age: 58,
  job: "Berufsfahrerin",
  gender: 'F',
  location: 'Italy',
  answer: [{
    language: 'de',
    text: "Rituale im Alltag täglich pflegen: Meditation, Körperpflege/Massage/Sonnenbaden, ernährungsbewusstes Kochen, Lesen, Aufräumen/Ordnung halten, genügend Schlaf, Kontakte zu Freunden und Familie pflegen",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE4',
  age: 60,
  job: "Krankenpfleger/in",
  gender: 'F',
  location: 'Italy',
  answer: [{
    language: 'de',
    text: "Zeit für Kleinigkeiten; \
    Zeit stehen zubleiben und den Vögeln zuzuhören; \
    Die Ruhe in der Umgebung genießen, keine laute Musik auf dem Spielplatz bis in den späten Abendstunden; \
    Weniger Programm und Abläufe; \
    Sich den Luxus zu gönnen langsam unterwegs zu sein, in jedem Sinne; \
    Weniger Alltagsstress; \
    Mehr Zeit zum Nachdenken u.a. über globalen Kapitalismus und globale Zusammenhänge;\
    Mehr lokale Produkte kaufen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE5',
  age: 73,
  job: "pensionierter Professor",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "täglich Gymnastik machen, Zeit für regelmäßig Bücher lesen, Regelmässig mit Freunden kommunizieren",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE6',
  age: 66,
  job: "Literaturuebersetzerin, Sprachlehrerin",
  gender: 'F',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Bewusstheit im Augenblick, Aufmerksamkeit, Konzentration, konkretes Interesse am Wohlergehen von Familienangehörigen, Freunden und Bekannten, Dankbarkeit",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE7',
  age: 21,
  job: "Studentin",
  gender: 'F',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Meine halbe Stunde work out mit meiner Schwester! ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE8',
  age: 26,
  job: "Studentin",
  gender: 'F',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: "Meditation, Yoga, Tagebuch, Lesen, Stricken, Tee trinken mit Familie (alles voher schon gemacht aber jetzt intensiver)",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE9',
  age: 25,
  job: "Studentin",
  gender: 'F',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: "Geocaching",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE10',
  age: 29,
  job: "Angestellter an der Universität ",
  gender: 'M',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: "Angeeignete Home-Office Skills, workout zuhause, mehr zu kochen, mehr videokonferenzen mit meiner familie",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE11',
  age: 56,
  job: "Körpertherapeutin",
  gender: 'F',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Die Dinge langsam angehen",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE12',
  age: 68,
  job: "Pensioniert",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Mehr Gelassenheit, mehr Einfachheit, weniger Hektik, mehr Gemütlichkeit; \
    Mehr Kontakte mit Nachbarn; \
    Mehr Kontakte mit entfernten Freunden über www; \
    Täglich Qi Gong, Feldenkrais und Muskeltraining; \
    Mehr Lernen mittels Webinare, Moocs, Themengruppen auf den Social Medien; \
    Mehr den Balkon genießen, besonders an sonnigen Tagen;  \
    ...\
    In einem Jahr werde ich zurückschauen und feststellen, was ich beibehalten habe. \
    Danke Reinhard für dieses Experiment!",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE13',
  age: 71,
  job: "Pensionist",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "hygienische Aspekte: die Hände im Laufe des Tages häufiger waschen, Vorsicht beim Küssen rechts und links und noch einmal rechts, den Händedruck weniger oft vornehmen (ganz so wie die Amerikaner), eine Gesichtsmaske immer dann tragen, wenn ich nicht ganz gesund bin (z.B. bei einer Erkältung oder Grippeanfall) und noch penibler in Sachen Hygiene sein beim Umgang mit anderen Menschen \
    Mobilität: weniger verkehren \
    Einkauf: einen fast schon alltäglichen Gang in den Supermarkt vermeiden und noch gezielter einkaufen \
    Kommunikation: Während der Quarantäne hat sich die Kommunikation  mit meinen Töchtern stark verbessert; das möchte ich beibehalten ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE14',
  age: 66,
  job: "Hotelier",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Mehr Zufriedenheit mit dem was man hat; \
    weniger Flugreisen; \
    Stärkung des sanften Tourismus; \
    eine Steuerpolitik, die die Unternehmen nicht zwingt, dauernd Neuinvestitionen zu tätigen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE15',
  age: 40,
  job: "Angestellte im Gastgewerbe und Vollzeit-Mama",
  gender: 'F',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "die Ruhe genießen und sich auch in den hektischen Zeiten mal eine Auszeit genehmigen; \
    Online-Watten (auf watten.org) -  macht wirklich Spass und fördert die Gehirnzellen; \
    mit den Kindern Trampolinspringen -  hält fit!",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE16',
  age: 58,
  job: "beamter",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "regelmäßiges und langsameres essen, so wie jetzt. mehr zeit für gartenarbeit, auch unwichtige. mehr und längeren kontakt mit freunden über´s chat u videochat. zu hause auch weiterhin zum teil in smartworking arbeiten. ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE17',
  age: 58,
  job: "Sozialarbeiter",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "die Stille, vor allem jene am Abend auf der Terrasse, kaum Straßenlärm, die gute Luft",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE18',
  age: 25,
  job: "Student",
  gender: 'M',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: "Durch die Quarantäne habe ich es verstärkt geschafft mich um meine innere Gesundheit zu kümmern. Durch die erzwungene soziale Distanz musste ich mich verstärkt mit mir selbst beschäftigen, wodurch mir einige Verhaltensweisen und Muster bewusst wurden, die ich in Zukunft verändern möchte. Diese Gelegenheit nutze ich gerade, da es für mich derzeit keine Verpflichtungen gibt. Diese freie Zeit bewusst zu nutzen finde ich sehr angenehm. Dies will ich mir auch für die Zeit nach der Quarantäne beibehalten um die Freizeit, die dann sicherlich weniger ist, bewusster geniesen zu können. Außerdem will ich mich weiterhin verstärkt selbst erforschen, um weitere Schritte machen zu können.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE19',
  age: 32,
  job: "Angestellte",
  gender: 'F',
  location: 'Japan',
  answer: [{
    language: 'de',
    text: "In japan gilt (noch) keine Ausgangssperre, das heist ich kann mich noch nicht genau dazu ausern, aber wir waschen uns gerade zu Hause oefter am Tag die Haende. Also was Hygene angeht moechte ich das auch nach 31-12-2020 beibehalten.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE20',
  age: 64,
  job: "Führungskraft",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Verschlankung, Verlangsamung",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE21',
  age: 69,
  job: "Pensionist",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Klare Tageszeitstruktur. Das wichtigste des Tages kommt zwischen 7 und 11 Uhr Vormittag. Das Handy ist ausgeschaltet. Lektüre, Nachdenken, zu Papier bringen. Dann Haushalt, dann Freunde, dann Essen zu Hause, dann Haushalt, dann Kontakte und Arbeit für Genossenschaft, Familie, Freunde. Wir haben alles und mehr als wir brauchen im materiellen Sinne. Die Ruhe tut gut. In ihr ist Kraft und Einfachheit. ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE22',
  age: 68,
  job: "Pensionist",
  gender: 'M',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "im spannungsfeld \
    zwischen hoffen und bangen \
    gelassenheit und verzweiflung \
    fällt mir  \
    Heralits \
    panta rhei\
    ein...\
    es gibt kein vorher\
    und kein nachher\
    alles nämlich fließt\
    und ich und wir mit allem.\
    vor 14 miliarden jahren\
    hat diese bewegung,\
    diese ausdehnung,\
    dieser fluss\
    begonnen.\
    es ist gut\
    mich als teil\
    dieser flusses\
    zu fühlen\
    und meine/unsere\
    verzweifelte versuchung,\
    diese bewegung \
    stoppen zu wollen\
    in der hoffnung\
    auf stabilität\
    und sicherheit\
    als sinnlos zu erkennen.\
    ...wenn alles vorbei\
    sein wird\
    wird der fluss weiter fließen\
    und ich werde \
    mich mehr denn je\
    als teil dieses flusses \
    erleben.\
    gelassenheit\
    ruhe \
    entspannung\
    verbundenheit...\
    wie einfach \
    es\
    doch ist",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE23',
  age: 72,
  job: "Rentnerin",
  gender: 'F',
  location: 'Germany',
  answer: [{
    language: 'de',
    text: "Nur wenige, da ich nicht mit meinen Enkelkindern und nur wenigen Freunden zusammen sein kann, ansonsten möchte ich weiterhin viel in der Natur und im Garten sein, Musik hören und Bücher lesen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE24',
  age: 67,
  job: "Beraterin in Pension",
  gender: 'F',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: " Mir ist bewusst geworden, wie kostbar meine Lebenszeit ist, vor allem in einem Alter, in dem die Endlichkeit zunehmend Thema wird. Fühle mich in meiner Lebenszeit im Moment bestohlen durch die Beschränkungen. Werde danach noch bewusster als vorher meine Tage gestalten und die Zeit noch besser nutzen, die mir in Gesundheit und Beweglichkeit bleibt. So habe ich diesen Aspekt nicht während der Quarantäne ausprobiert, sondern die Wichtigkeit ist mir durch die Abwesenheit dieses Aspekts erst bewusst geworden.Ebenso die Bedeutung der körperlichen Berührung innerhalb der Familie. Das, was vorher selbstverständlich war- nämlich sich zu umarmen innerhalb der Familie- geht mir nun schmerzlich ab. Dies können auch die vielen Videotelefonate nicht ersetzen. Im Gegenteil- es wird mir schmerzlich bewusst, wie sehr mir das fehlt. ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE25',
  age: 61,
  job: "Künstler",
  gender: 'M',
  location: 'Germany',
  answer: [{
    language: 'de',
    text: "Mehr Zeit für meine Interessen. Mehr Ruhe und weniger Bearf an Dingen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE26',
  age: 77,
  job: "Schauspiellehrer. Begründer der Methode Slow Acting. Coach.",
  gender: 'M',
  location: 'Germany',
  answer: [{
    language: 'de',
    text: "Tatsächlich geht es mir verstärkt darum dieses andere Sein und Da-Sein beizubehalten. Nämlich die Zeit zu verlangsamen, sie zu dehnen. Dazu gehören tägliche aktive und passive Meditationsübungen, das aktive Imaginieren und vor allem: Weiterschreiben an meinem Buch Slow Acting, das vor der Quarantäne seit Monaten unterbrochen war. Ganz neu in mein Leben getreten ist die Möglichkeit Potcast zu gestalten. Diese Möglichkeit möchte ich weiter ausbauen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE27',
  age: 52,
  job: "Landesangestellte",
  gender: 'F',
  location: 'Italy',
  answer: [{
    language: 'de',
    text: "Heimtraining (allg. Fitness)  \
    Kochen (keine Schnellgerichte, Selbszubereitung von Pizza oder alte Rezepte von Mutti: Schlutzkrapfen, Kartoffel-Riebler, Christenwürger usw. )\
    Backen (Feiertagsgebäck, Kekse und Kuchen) \
    im Freien auf der Terasse:\
    - Frühstücken, Mittag- und Abendessen\
    - Grillen\
    - Smalltalk mit Nachbarn\
    - Sonnetanken im Liegestuhl\
    - Sterne- und Mondbeobachtung, Geschichtenerzählen, Singen, Gitarrespielen, Kuscheln\
    Gesellschaftsspiele (neue und alte traditionelle Spiele wie z.B.Tabu/Aktivity oder Mensch-Ärgere-Dich-Nicht, TOMBOLA, Kartenspiele: Watten, Spitzen, MauMau, Remie, Poker usw. oder computerunterstützte Spiele wie PS - Playstation) ",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE28',
  age: 39,
  job: "Pädagogin / Fotografin",
  gender: 'F',
  location: 'Austria',
  answer: [{
    language: 'de',
    text: "Ich mag die Entschleunigung, ich habe den Eindruck, dass ich meine Beziehungen intensiver lebe & das macht mir Freude, besonders das Familienleben ist entspannt und tief verbunden. Ich konsumiere wenig und das passt sehr gut so.", 
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}, {
  id: 'DE29',
  age: 18,
  job: "Studentin",
  gender: 'F',
  location: 'Italia',
  answer: [{
    language: 'de',
    text: "Ich möchte weiterhin Fitness und Yoga zuhause machen.",
    original: true
  },{
    language: 'it',
    text: ''
  },{
    language: 'en',
    text: ''
  },]
}]