import answers from './answers'

import categories from './categories'

export {
  answers,
  categories
}