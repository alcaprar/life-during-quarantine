export default [{
  id: 1,
  label: {
    it: 'Volersi bene e cura del sé',
    de: 'Gesund Leben',
    en: 'Love yourself'
  },
  keywords: {
    it: 'passione per la cucina, alimentarsi consapevolmente; prodotti locali, workout, benessere psicofisico, capacità di stare da soli, ricerca interiore, igiene e salute;',
    de: 'Lust zu Kochen, gesund essen, lokale Produkte, Training, psychophysisches Wohlbefinden, die Fähigkeit allein zu sein, in mich gehen, Hygiene und Gesundheit.',
    en: ' passion cooking, eat local products, workout, invest in your mental-body wellbeing, be on your own, take a look inside yourself, hygiene and health'
  },
  song: {
    label: 'Alex Britti - La vasca',
    url: 'https://www.youtube.com/watch?v=Asuw3J4-tJo'
  },
  answers: ["EN2",
  "EN5",
  "EN6 ",
  "EN7",
  "EN8",
  "EN9",
  "EN10",
  "EN12",
  "EN15",
  "EN19",
  "IT2",
"IT18",
"IT31",
"IT43",
"IT50",
"IT62",
"IT87","DE2",
"DE3",
"DE4",
"DE5",
"DE7",
"DE10",
"DE16",
"DE18",
"DE20",
"DE27"

]
},{
  id: 2,
  label: {
    it: 'Il tempo ritrovato',
    de: 'Zeit, die mir gehört',
    en: 'Finding new pockets of times'
  },
  keywords: {
    it: 'ritorno a ritmi umani;  apprezzare la lentezza, dedicarsi alle piccole cose, avere più tempo; seguire gli obiettivi personali; curare hobby, hic et nunc, autonomia,  libertà di scelta, meditazione, silenzio; ',
    de: 'menschliche Rhythmen; Wert der Langsamkeit; die kleinen Dinge achten; Zeitsouveränität;  persönliche Ziele verfolgen; Hobbys pflegen, Hier und Jetzt; Selbstbestimmung; Entscheidungsfreiheit;  Meditation; Ruhe.',
    en: 'red thread: back to more human rhythms and enjoy slow sailing, caring about the little things, time for personal development, hobbies, hic et nunc, freedom of choice, meditation, silence'
  },
  song: {
    label: 'Franco Battiato: L\'oceano di silenzio',
    url: 'https://www.youtube.com/watch?v=DN2Pdulrrog'
  },
  answers: ['EN2', "IT3",
  "IT6",
  "IT7",
  "IT13",
  "IT16",
  "IT 19",
  "IT26",
  "IT31",
  "IT34",
  "IT41",
  "IT42",
  "IT47",
  "IT61",
  "IT66",
  "IT67",
  "IT71",
  "IT72",
  "IT77",
  "IT78",
  "IT82",
  "IT83",
  "IT84",
  "IT85","DE4",
  "DE6",
  "DE8",
  "DE11",
  "DE12",
  "DE13",
  "DE19",
  "DE15",
  "DE20",
  "DE22",
  "DE25",
  "DE26"
  ]
}, {
  id: 3,
  label: {
    it: 'L\'essenziale',
    de: 'Zufrieden sein, mit..',
    en: 'Strict necessities'
  },
  keywords: {
    it: 'Consumare meno, minimalismo, essere contenti di quello che si ha, ridurre la mobilità, slow turismo, seguire quello che conta veramente.  ',
    de: 'Weniger Konsum; Minimalismus; Zufrieden Sein mit dem was eine/r hat; Mobilitäts Reduktion; sanfter Tourismus; Raum für das, was wirklich zählt. ',
    en: 'to use less, minimalism, be happy with less, reduced mobility, slow tourism, follow what really matters'
  },
  song: {
    label: 'Marco Mengoni: L\'essenziale.',
    url: 'https://www.youtube.com/watch?v=unRjK82bDLw'
  },
  answers: ['EN3', 'EN4', 'IT17', 'IT39', 'IT57', 'IT72', 'IT74', 'DE13', 'DE14','DE21','DE24','DE25']
}, {
  id: 4,
  label: {
    it: 'Nutrire lo spirito',
    de: 'Das Grundbedürfnis Kultur',
    en: 'Farm your spirit.'
  },
  keywords: {
    it: 'leggere, ascoltare musica; l\'importanza dell\'estetica; il gioco;  suonare uno strumento. ',
    de: 'Lesen, Musikhören, Wert der Ästhetik; Spielen; musizieren. ',
    en: 'read, listen to music, play instruments, the importance of aesthetics and of playing.'
  },
  song: {
    label: 'Carmen Consoli: Autunno dolciastro.',
    url: 'https://www.youtube.com/watch?v=-prmFHDHZhY'
  },
  answers: ['EN11','EN13','EN14','EN15', 'IT4','IT8','IT11','IT12','IT39','IT43','IT48','IT62','IT76','IT86','IT87','DE3','DE5','DE9','DE15','DE23','DE27']
},{
  id: 5,
  label: {
    it: 'Riallacciare i fili',
    de: 'Kontakte (wieder) pflegen',
    en: 'Building bridges'
  },
  keywords: {
    it: 'coltivare i contatti con persone lontane;  lunghe telefonate - chat- video messaggi -internet; ricordarsi di amicizie remote, riscoprire socialità dimenticate;',
    de: 'Kontakte mit entfernten Bekannten pflegen; langes Telefonieren-Chatten-Videotelefonie-Internet; Erinnerung an alte Freundschaften; vergessene Sozialkontakte wiederentdecken. ',
    en: 'nurture your network through long phone calls, chats, video calls, remember far away friends and rediscover forgotten relations.'
  },
  song: {
    label: 'Lucio Dalla: Caro amico ti scrivo.',
    url: 'https://www.youtube.com/watch?v=Y8HfQ7C8NFQ'
  },
  answers: ['EN16','EN17','EN19', 'IT4','IT8','IT27','IT39','IT67','IT69', 'DE3', 'DE5','DE12','DE13','DE16']
},{
  id: 6,
  label: {
    it: 'Il focolare domestico',
    de: 'Zuhause und Familienleben',
    en: 'Home sweet home'
  },
  keywords: {
    it: 'voglia di famiglia, cura della casa, ritrovarsi nel nucleo familiare ristretto, il dialogo in famiglia, meno attività esterne.',
    de: 'Lust auf Familie, Häuslichkeit; im engen Familienkreis zusammenkommen; Familiengespräche; weniger außerhäusliche Beschäftigung.',
    en: 'longing for family, take care of your home, being together, communications in the family, spending little time outside.'
  },
  song: {
    label: 'Roberto Vecchioni: Chiamami ancora amore.',
    url: 'https://www.youtube.com/watch?v=P5mgQ7Sk2Gw'
  },
  answers: ['EN18', "IT2",
  "IT4",
  "IT18",
  "IT23",
  "IT30",
  "IT33",
  "IT40",
  "IT45",
  "IT46",
  "IT47",
  "IT52",
  "IT53",
  "IT56",
  "IT58",
  "IT59",
  "IT60",
  "IT75",
  "IT76",
  "IT79",
  "IT80",
  "IT83", "DE6"]
}, {
  id: 7,
  label: {
    it: 'Immersioni digitali',
    de: 'Online arbeiten und lernen',
    en: 'Digital revolution'
  },
  keywords: {
    it: 'cambiare le modalità di lavorare e di frequentare la scuola, lavoro flessibile, didattica a distanza. ',
    de: 'Arbeitsmodalitäten ändern; Schule anders; flexible Arbeitsmodelle; Online-Unterricht',
    en: ' red thread: changing your work and study habits, flexible working, remote teaching.'
  },
  song: {
    label: 'KRAFTWERK: “Computer love”',
    url: 'https://www.youtube.com/watch?v=uNBGWenPlGo'
  },
  answers: ["IT14",
  "IT17",
  "IT18",
  "IT 21",
  "IT22",
  "IT25",
  "IT32",
  "IT35",
  "IT55",
  "IT64",
  "IT65",
  "IT68",
  "IT73",
  "IT81",
  "IT87", 'DE10', 'DE16']
}, {
  id: 8,
  label: {
    it: 'IO&NOI in un mondo sostenibile',
    de: 'Gemeinschaftssinn und Umwelt',
    en: 'Being, in a sustainable environment '
  },
  keywords: {
    it: 'solidarietà e altruismo, senso civico, essere responsabile per la comunità e l\'ambiente, cura ambientale. ',
    de: 'Solidarität und Altruismus; Bürgersinn; Verantwortung für das Gemeinwesen und die Umwelt; Umweltpflege.',
    en: 'solidarity and altruism, being good citizen, being responsible for your community and the environment, environmental care.'
  },
  song: {
    label: 'Marco Mengoni: Esseri umani',
    url: 'https://www.youtube.com/watch?v=U-4OrzSBfm8'
  },
  answers: ["IT17",
  "IT18",
  "IT20",
  "IT35",
  "IT38",
  "IT55",
  "IT68",
  "IT71",
  "IT74",
  "IT75",
  "IT84","DE2",
  "DE13",
  "DE14",
  "DE16",
  "DE17",
  "DE23"
  ]
},  {
  id: 9,
  label: {
    it: 'Tutto – niente',
    de: 'Alles-Nichts und viel dazwischen',
    en: 'What lies between everything and nothing'
  },
  keywords: {
    it: 'non salvare niente, ripartire con un mondo completamente nuovo, tutto, tutto è in divenire. ',
    de: 'Nichts; Beginn einer neuen Welt; Alles; Alles fließt.',
    en: 'red thread: do not save anything, restart in a new world, there is nothing I like about this, panta rei.'
  },
  song: {
    label: 'Vasco Rossi: Un senso',
    url: 'https://www.youtube.com/watch?v=StRtFh01XUo'
  },
  answers: ["IT7",
  "IT9",
  "IT10",
  "IT15",
  "IT24",
  "IT28",
  "IT29",
  "IT36",
  "IT37",
  "IT49",
  "IT51",
  "IT54",
  "IT63",
  "IT70",
  "IT73", 'DE22'
  ]
}]